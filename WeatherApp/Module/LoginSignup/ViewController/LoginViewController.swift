//
//  LoginViewController.swift
//  WeatherApp
//
//  Created by apple on 07/10/21.
//

import UIKit
import CoreLocation
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    
    var email: String?
    var password: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setup() {
        self.emailTf.delegate = self
        self.passwordTf.delegate = self
        self.textFieldStyle(textField: emailTf)
        self.textFieldStyle(textField: passwordTf)
        self.passwordTf.isSecureTextEntry = true
        self.emailTf.setLeftPaddingPoints(10)
        self.passwordTf.setLeftPaddingPoints(10)
    }
    
   
    
    func textFieldStyle(textField: UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor(red: 16, green: 129, blue: 255, alpha: 1).cgColor
        textField.layer.cornerRadius = 5
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    @IBAction func loginButtonTap(_ sender: Any) {
        Auth.auth().signIn(withEmail: email ?? "", password: password ?? "") { (authResult, error) in
          if let error = error as? NSError {
            switch AuthErrorCode(rawValue: error.code) {
            case .operationNotAllowed: break
              // Error: Indicates that email and password accounts are not enabled. Enable them in the Auth section of the Firebase console.
            case .userDisabled: break
              // Error: The user account has been disabled by an administrator.
            case .wrongPassword: break
              // Error: The password is invalid or the user does not have a password.
            case .invalidEmail: break
              // Error: Indicates the email address is malformed.
            default:
                print("Error: \(error.localizedDescription)")
            }
          } else {
            print("User signs in successfully")
            let userInfo = Auth.auth().currentUser
            let email = userInfo?.email
            let tabBarVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
            let sceneDelegate = UIApplication.shared.connectedScenes
                    .first!.delegate as! SceneDelegate
            sceneDelegate.window!.rootViewController = tabBarVC
            
          }
        }
//        let tabBarVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
//        self.navigationController?.pushViewController(tabBarVC, animated: true)
    }
    
    @IBAction func signInButtonTap(_ sender: Any) {
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate , CLLocationManagerDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
                   let textRange = Range(range, in: text) {
                   let updatedText = text.replacingCharacters(in: textRange,with: string)
            print(updatedText)
            if textField == emailTf {
                self.email = updatedText
                if isValidEmail(updatedText) {
                    print("Email is Valid")
                } else {
                    print("Email is Invalid")
                }
            } else {
                self.password = updatedText
            }
        }
        return true
    }
    
    
}

