//
//  SignupViewController.swift
//  WeatherApp
//
//  Created by apple on 07/10/21.
//

import UIKit
import FirebaseAuth

class SignupViewController: UIViewController {

    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var passTf: UITextField!
    @IBOutlet weak var confirmPassTf: UITextField!
    
    var textFieldArray = [UITextField]()
    var handle = AuthStateDidChangeListenerHandle?.self
    var email: String?
    var password: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    func setup() {
        self.textFieldArray = [nameTf, emailTf, passTf, confirmPassTf]
        for field in textFieldArray {
            setupTextField(textField: field)
        }
        self.passTf.isSecureTextEntry = true
        self.confirmPassTf.isSecureTextEntry = true
    }
    
    func setupTextField(textField: UITextField) {
        textField.setLeftPaddingPoints(10)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor(red: 16, green: 129, blue: 255, alpha: 1).cgColor
        textField.delegate = self
        textField.layer.cornerRadius = 5
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    @IBAction func signupButtonTap(_ sender: Any) {
        
        Auth.auth().createUser(withEmail: email ?? "", password: password ?? "") { authResult, error in
          if let error = error as? NSError {
            switch AuthErrorCode(rawValue: error.code) {
            case .operationNotAllowed: break
              // Error: The given sign-in provider is disabled for this Firebase project. Enable it in the Firebase console, under the sign-in method tab of the Auth section.
            case .emailAlreadyInUse: break
              // Error: The email address is already in use by another account.
            case .invalidEmail: break
              // Error: The email address is badly formatted.
            case .weakPassword: break
              // Error: The password must be 6 characters long or more.
            default:
                print("Error: \(error.localizedDescription)")
            }
          } else {
            print("User signs up successfully")
            let newUserInfo = Auth.auth().currentUser
            let email = newUserInfo?.email
            self.navigationController?.popViewController(animated: true)
          }
        }
        
    }
    

}

extension SignupViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
                   let textRange = Range(range, in: text) {
                   let updatedText = text.replacingCharacters(in: textRange,with: string)
            print(updatedText)
            if textField == emailTf {
                self.email = updatedText
                if isValidEmail(updatedText) {
                    print("Valid")
                } else {
                    print("Invalid")
                }
            } else {
                self.password = updatedText
            }
        }
        return true
    }
}
