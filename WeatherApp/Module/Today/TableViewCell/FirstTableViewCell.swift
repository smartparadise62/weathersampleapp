//
//  FirstTableViewCell.swift
//  WeatherApp
//
//  Created by apple on 07/10/21.
//

import UIKit

class FirstTableViewCell: UITableViewCell {

    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getData(weatherData: WeatherModel) {
        
    }

}
