//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by apple on 08/10/21.
//

import Foundation

class WeatherModel: Codable {
    var cod:String
    var message: Int?
    var city: CityData?
    var list: [ListModelData]?
    
}

class CityData: Codable {
    var country, name:String?
    var sunrise, sunset, timezone: Double?
    var id, population: Double?
    var coord: LatLongData?
}

class LatLongData: Codable {
    var lat, lon: Double?
    
}

class ListModelData: Codable {
    var dt, visibility: Double?
    var dt_txt: String?
    var main: MainData?
    var clouds: CloudData?
    var weather: [WeatherModelData]?
    var wind: WindData?
    
}

class WeatherModelData: Codable {
    var description, icon, main: String?
    var id: Double?
}

class CloudData: Codable {
    var all: Double?
}
class MainData: Codable {
    var feels_like, temp, temp_max, temp_min: Double?
    var grnd_level, humidity, pressure, sea_level, temp_kf: Double?
}

class WindData: Codable {
    var gust, speed: Double?
    var deg: Double?
}
