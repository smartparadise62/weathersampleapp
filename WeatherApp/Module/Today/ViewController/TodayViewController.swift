//
//  TodayViewController.swift
//  WeatherApp
//
//  Created by apple on 07/10/21.
//

import UIKit
import CoreLocation


class TodayViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let locationManager = CLLocationManager()
    var lat: String = ""
    var long: String = ""
    var param = [String: AnyObject]()
    var weatherData: WeatherModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.setupUserLocation()
         
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    func setupUserLocation() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    func callApi(_ lat: String,_ lon:String) {
        let headers = [
            "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
            "x-rapidapi-key": "0dd446fdbemshf61898474dcc6f4p19cf3fjsn7614728ac2f9"
        ]

        let request = NSMutableURLRequest(url: NSURL(string: "https://community-open-weather-map.p.rapidapi.com/forecast?lat=\(lat)&lon=\(lon)")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        print(request)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                if let res = data as? [String:AnyObject] {
                    print(res)
                }
                do {
                    
                    let weatherModel = try JSONDecoder().decode(WeatherModel.self, from: data!)
                    self.weatherData = weatherModel
                } catch let error {
                    print(error)
                }
            }
        })

        dataTask.resume()
    }
}

extension TodayViewController: UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FirstTableViewCell", for: indexPath) as? FirstTableViewCell else {
                fatalError()
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SecondTableViewCell", for: indexPath) as? SecondTableViewCell else {
                fatalError()
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ThirdTableViewCell", for: indexPath) as? ThirdTableViewCell else {
                fatalError()
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        let lat = "\(locValue.latitude)"
        let long = "\(locValue.longitude)"
        self.lat = lat
        self.long = long
        callApi(lat, long)
        locationManager.stopUpdatingLocation()
    }
}
